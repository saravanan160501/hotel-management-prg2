﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2assignment
{
    class DeluxeRoom:HotelRoom
    {
        private bool additionalBed;

        public bool AdditionalBed
        {
            get { return additionalBed; }
            set { additionalBed = value; }
        }

        public DeluxeRoom() { }

        public DeluxeRoom(string rt, string rn,string bc,int dr, bool ia, int noo)
        {
            RoomType = rn;
            RoomNumber = rn;
            BedConfiguration = bc;
            DailyRate = dr;
            IsAvail = ia;
            NoOfOccupants = noo;
        }

        public override double CalculateCharges()
        {
            double total = DailyRate;

           if (additionalBed == true)
            {
                total += 25;
            }
            return total;
        }

        public override string ToString()
        {
            return "\nAdditional Bed Used: " + additionalBed;
        }
    }
}
