﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.UI.Popups;
using System.Text;
using System.Threading.Tasks;



namespace PRG2assignment
{
    public sealed partial class MainPage : Page
    {
        List<Guest> guestDetailList = new List<Guest>();
        List<HotelRoom> hotelRoomList = new List<HotelRoom>();
        List<HotelRoom> selectedRoomItemList = new List<HotelRoom>();


        public MainPage()
        {
            this.InitializeComponent();
            InitData();
            ValidateMaster();
        }


        /*Initialise Data*/
        public void InitData()
        {
            /*Initialise hotelRoomList Data*/
            hotelRoomList.Add(new StandardRoom { RoomType = "Standard", RoomNumber = "102", BedConfiguration = "Single", DailyRate = 90, IsAvail = true, NoOfOccupants = 0 });
            hotelRoomList.Add(new StandardRoom { RoomType = "Standard", RoomNumber = "201", BedConfiguration = "Twin", DailyRate = 110, IsAvail = true, NoOfOccupants = 0 });
            hotelRoomList.Add(new StandardRoom { RoomType = "Standard", RoomNumber = "203", BedConfiguration = "Twin", DailyRate = 110, IsAvail = true, NoOfOccupants = 0  });
            hotelRoomList.Add(new DeluxeRoom { RoomType = "Deluxe", RoomNumber = "204", BedConfiguration = "Twin", DailyRate = 140, IsAvail = true, NoOfOccupants = 0 });
            hotelRoomList.Add(new DeluxeRoom { RoomType = "Deluxe", RoomNumber = "205", BedConfiguration = "Twin", DailyRate = 140, IsAvail = true, NoOfOccupants = 0 });
            hotelRoomList.Add(new StandardRoom { RoomType = "Standard", RoomNumber = "301", BedConfiguration = "Triple", DailyRate = 120, IsAvail = true, NoOfOccupants = 0 });
            hotelRoomList.Add(new DeluxeRoom { RoomType = "Deluxe", RoomNumber = "304", BedConfiguration = "Triple", DailyRate = 210, IsAvail = true, NoOfOccupants = 0 });

            /*Initialise Stay Data*/
            Stay s1 = new Stay(new DateTime(2019, 01, 26), new DateTime(2019, 02, 02));
            Stay s2 = new Stay(new DateTime(2019, 01, 25), new DateTime(2019, 01, 31));
            Stay s3 = new Stay(new DateTime(2019, 02, 01), new DateTime(2019, 02, 06));
            Stay s4 = new Stay(new DateTime(2019, 01, 28), new DateTime(2019, 02, 10));

            s1.AddRoom(new StandardRoom { RoomType = "Standard", RoomNumber = "101", BedConfiguration = "Single", DailyRate = 90, IsAvail = false, NoOfOccupants = 1 });
            s2.AddRoom(new StandardRoom { RoomType = "Standard", RoomNumber = "302", BedConfiguration = "Triple", DailyRate = 9120, IsAvail = false, NoOfOccupants = 3 }); ;
            s3.AddRoom(new StandardRoom { RoomType = "Standard", RoomNumber = "202", BedConfiguration = "Twin", DailyRate = 110, IsAvail = false, NoOfOccupants = 2 });
            s4.AddRoom(new DeluxeRoom { RoomType = "Deluxe", RoomNumber = "303", BedConfiguration = "Triple", DailyRate = 210, IsAvail = false, NoOfOccupants = 4 });



            /*Initialise Guest Data*/
            guestDetailList.Add(new Guest { Name = "Amelia", PpNumber = "S1234567A", HotelStay = s1, Membership = new Membership { Status = "Gold", Points = 280 }, IsCheckedIn = true });
            guestDetailList.Add(new Guest { Name = "Bob", PpNumber = "G1234567A", HotelStay = s2, Membership = new Membership { Status = "Ordinary", Points = 0 }, IsCheckedIn = true });
            guestDetailList.Add(new Guest { Name = "Cody", PpNumber = "G2345678A", HotelStay = s3, Membership = new Membership { Status = "Silver", Points = 190 }, IsCheckedIn = true });
            guestDetailList.Add(new Guest { Name = "Edda", PpNumber = "S3456789A", HotelStay = s4, Membership = new Membership { Status = "Gold", Points = 10 }, IsCheckedIn = true });

            ((StandardRoom)guestDetailList[0].HotelStay.RoomList[0]).RequireBreakfast = true;
            ((StandardRoom)guestDetailList[0].HotelStay.RoomList[0]).RequireWifi = true;
            ((StandardRoom)guestDetailList[1].HotelStay.RoomList[0]).RequireBreakfast = true;
            ((StandardRoom)guestDetailList[2].HotelStay.RoomList[0]).RequireBreakfast = true;
            ((DeluxeRoom)guestDetailList[3].HotelStay.RoomList[0]).AdditionalBed = true;

            guestDetailList[0].HotelStay.RoomList[0].IsCheckedIn = true;
            guestDetailList[1].HotelStay.RoomList[0].IsCheckedIn = true;
            guestDetailList[2].HotelStay.RoomList[0].IsCheckedIn = true;
            guestDetailList[3].HotelStay.RoomList[0].IsCheckedIn = true;
        }

        /*Methods*/
        /*Show Message -- MessageBox.Show("Text", "Header");*/
        public static class MessageBox
        {
            static public async void Show(string header, string text)
            {
                var dialog = new MessageDialog(text, header);
                await dialog.ShowAsync();
            }
        }


        /*Validation Functions*/
        /*Validation Master*/
        public void ValidateMaster()
        {
            ValidateCheckRoomsAvailableButton();
            ValidateSearchButton();
            ValidateSidebar();
            ValidateRemoveRoomButton();
            ValidateCheckInButton();
            ValidateCheckOutButton();
        }

        /*Check Availability Button*/
        public void ValidateCheckRoomsAvailableButton()
        {
            if (String.IsNullOrEmpty(noOfAdultsTextBox.Text) || (String.IsNullOrEmpty(noOfChildrenTextBox.Text)))
            {
                checkAvailabilityButton.IsEnabled = false;
            }
            else
            {
                checkAvailabilityButton.IsEnabled = true;
            }
        }

        /*Search Button*/
        public void ValidateSearchButton()
        {
            if (String.IsNullOrEmpty(guestPassportNumberTextBox.Text))
            {
                searchButton.IsEnabled = false;
            }
            else
            {
                searchButton.IsEnabled = true;
            }
        }

        /*Room Options Sidebar (Checkboxes and Add Room Button)*/
        public void ValidateSidebar()
        {
            if (availableRoomsListView.Items.Count() == 0)
            {
                addBreakfastCheckBox.IsEnabled = false;
                addWifiCheckBox.IsEnabled = false;
                addBedCheckBox.IsEnabled = false;
                addRoomButton.IsEnabled = false;
            }
            else
            {
                addBreakfastCheckBox.IsEnabled = true;
                addWifiCheckBox.IsEnabled = true;
                addBedCheckBox.IsEnabled = true;
                addRoomButton.IsEnabled = true;
            }

            HotelRoom selectedRoomItem = (HotelRoom)availableRoomsListView.SelectedItem;
            CheckBox[] checkboxes = new CheckBox[] { addBreakfastCheckBox, addWifiCheckBox, addBedCheckBox };

            if (selectedRoomItem is StandardRoom)
            {
                checkboxes[2].IsEnabled = false;
            }
            else
            {
                checkboxes[0].IsEnabled = false;
                checkboxes[1].IsEnabled = false;
            }
        }

        /*Remove Room Button*/
        public void ValidateRemoveRoomButton()
        {
            if (roomsSelectedListView.Items.Count() == 0)
            {
                removeRoomButton.IsEnabled = false;
            }
            else
            {
                removeRoomButton.IsEnabled = true;
            }    
        }

        /*Check In Button*/
        public void ValidateCheckInButton()
        {
            int numberOfAdults = Convert.ToInt32(noOfAdultsTextBox.Text);
            int numberofChildren = Convert.ToInt32(noOfChildrenTextBox.Text);
            int totalNumberofAdults = numberOfAdults + numberofChildren * 2;
            int capacity = 0;

            foreach (HotelRoom h in selectedRoomItemList)
            {
                if (h.BedConfiguration == "Single")
                {
                    capacity += 1;
                }
                else if (h.BedConfiguration == "Twin")
                {
                    capacity += 2;
                }
                else if (h.BedConfiguration == "Triple")
                {
                    capacity += 3;
                }
                if (h.RoomType == "Deluxe" && ((DeluxeRoom)h).AdditionalBed == true)
                {
                    capacity += 1;
                }
            }

            totalRoomCapacityTextBox.Text = capacity.ToString();
            totalPaxTextBox.Text = totalNumberofAdults.ToString();

            if (String.IsNullOrEmpty(guestNameTextBox.Text) || String.IsNullOrEmpty(guestPassportNumberTextBox.Text) || checkInCalendarDatePicker.Date == null || checkOutCalendarDatePicker.Date == null || totalNumberofAdults > capacity)
            {
                checkInButton.IsEnabled = false;
                statusUpdateTextBox.Text = "Fill up the necessary informations or add more rooms.";
            }
            else if (totalNumberofAdults <= capacity)
            {
                checkInButton.IsEnabled = true;
                statusUpdateTextBox.Text = "Click on Refresh to update the content on your screen if they are not automatically done for you.";
            }
            else
            {
                checkInButton.IsEnabled = true;
                statusUpdateTextBox.Text = "";
            }
        }

        /*Check Out Button*/
        public void ValidateCheckOutButton()
        {
            checkOutButton.IsEnabled = false;
        }

        /*UI Event Listeners*/
        /*Refresh Button*/
        private void RefreshButton_Click(object sender, RoutedEventArgs e)
        {
            ValidateMaster();
        }

        /*Guest Name TextBox*/
        private void GuestNameTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            ValidateCheckInButton();
            ValidateSearchButton();
        }

        /*Guest Passport Number TextBox*/
        private void GuestPassportNumberTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            ValidateCheckInButton();
            ValidateSearchButton();
        }

        /*Adults TextBox*/
        private void NoOfAdultsTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            ValidateCheckRoomsAvailableButton();
        }

        /*Children TextBox*/
        private void NoOfChildrenTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            ValidateCheckRoomsAvailableButton();
        }

        /*Check-In Date*/
        private void CheckInCalendarDatePicker_DateChanged(CalendarDatePicker sender, CalendarDatePickerDateChangedEventArgs args)
        {
            ValidateCheckInButton();
        }

        /*Check-Out Date*/
        private void CheckOutCalendarDatePicker_DateChanged(CalendarDatePicker sender, CalendarDatePickerDateChangedEventArgs args)
        {
            ValidateCheckInButton();
        }

        /*Available Rooms ListView*/
        private void AvailableRoomsListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ValidateSidebar();
        }

        /*Roomes Selected ListView*/
        private void RoomsSelectedListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ValidateRemoveRoomButton();
            ValidateCheckInButton();
        }

        /*Extend Stay Button*/
        private void Button_Click_6(object sender, RoutedEventArgs e)
        {
            string guestPassportNumber = guestPassportNumberTextBox.Text;

            foreach (Guest g in guestDetailList)
            {
                if (g.PpNumber == guestPassportNumber.ToUpper())
                {
                    g.HotelStay.CheckOutDate = g.HotelStay.CheckOutDate.AddDays(1);
                }
            }
            Button_Click_4(sender, e);
        }



        /*Button Functions*/
        /*Check Availability Button*/
        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            hotelRoomList.RemoveAll(r => r.IsAvail == false);
            availableRoomsListView.ItemsSource = hotelRoomList;
        }

        /*Search Button*/
        private void Button_Click_4(object sender, RoutedEventArgs e)
        {
            checkOutButton.IsEnabled = true;
            invoiceTextBlock.Text = "";

            string guestPassportNumber = guestPassportNumberTextBox.Text;

            foreach (Guest g in guestDetailList)
            {
                if (g.PpNumber.ToUpper() == guestPassportNumber.ToUpper())
                {
                    memberStatusTextBox.Text = g.Membership.Status;
                    pointsAvailableTextBox.Text = g.Membership.Points.ToString();
                    availableRoomsListView.ItemsSource = g.HotelStay.RoomList;

                    foreach (HotelRoom h in g.HotelStay.RoomList)
                    {
                        invoiceTextBlock.Text = h.ToString();
                        invoiceTextBlock.Text += g.ToString();
                    }                   
                }
            }

            if (availableRoomsListView.Items.Count() == 0)
            {
                statusUpdateTextBox.Text =  "No Search Results Found, This error may occur when the searched user has not been registered before or when the user has already checkout.";              
            }
        }

        /*Add Room Button*/
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            HotelRoom selectedRoomItem = (HotelRoom) availableRoomsListView.SelectedItem;

            CheckBox[] checkboxes = new CheckBox[] { addBreakfastCheckBox, addWifiCheckBox, addBedCheckBox };
            if (selectedRoomItem is StandardRoom)
            {
                addBreakfastCheckBox.IsEnabled = false;

                if (checkboxes[0].IsChecked == true)
                {
                    ((StandardRoom)selectedRoomItem).RequireBreakfast = true;
                }
                if (checkboxes[1].IsChecked == true)
                {
                    ((StandardRoom)selectedRoomItem).RequireWifi = true;
                }
            }
            else
            {
                addBreakfastCheckBox.IsEnabled = false;
                addWifiCheckBox.IsEnabled = false;

                if (checkboxes[2].IsChecked == true)
                {
                    ((DeluxeRoom)selectedRoomItem).AdditionalBed = true;
                }
            }

            selectedRoomItem.DailyRate = selectedRoomItem.CalculateCharges();
            hotelRoomList.Remove(selectedRoomItem);
            selectedRoomItemList.Add(selectedRoomItem);
            availableRoomsListView.ItemsSource = null;
            roomsSelectedListView.ItemsSource = null;
            availableRoomsListView.ItemsSource = hotelRoomList;
            roomsSelectedListView.ItemsSource = selectedRoomItemList;
        }

        /*Remove Button*/
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            HotelRoom selectedRoomItem = (HotelRoom)roomsSelectedListView.SelectedItem;

            if (selectedRoomItem.BedConfiguration == "Single" && selectedRoomItem.RoomType == "Standard")
            {
                selectedRoomItem.DailyRate = 90;
            }

            else if (selectedRoomItem.BedConfiguration == "Twin" && selectedRoomItem.RoomType == "Standard")
            {
                selectedRoomItem.DailyRate = 110;
            }

            else if (selectedRoomItem.BedConfiguration == "Triple" && selectedRoomItem.RoomType == "Standard")
            {
                selectedRoomItem.DailyRate = 120;
            }

            else if (selectedRoomItem.BedConfiguration == "Twin" && selectedRoomItem.RoomType == "Deluxe")
            {
                selectedRoomItem.DailyRate = 140;
            }

            else if (selectedRoomItem.BedConfiguration == "Triple" && selectedRoomItem.RoomType == "Deluxe")
            {
                selectedRoomItem.DailyRate = 210;
            }

            hotelRoomList.Add(selectedRoomItem);
            selectedRoomItemList.Remove(selectedRoomItem);
            availableRoomsListView.ItemsSource = null;
            roomsSelectedListView.ItemsSource = null;
            availableRoomsListView.ItemsSource = hotelRoomList;
            roomsSelectedListView.ItemsSource = selectedRoomItemList;
        }

        /*Check-In Button*/
        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            List<Guest> newGuestList = new List<Guest>();

            string guestName = guestNameTextBox.Text;
            string guestPassportNumber = guestPassportNumberTextBox.Text;

            DateTime checkInDate = checkInCalendarDatePicker.Date.Value.DateTime;
            DateTime checkOutDate = checkOutCalendarDatePicker.Date.Value.DateTime;
            bool errorFlag = false;

            foreach (Guest g in guestDetailList)
            {
                if (g.PpNumber == guestPassportNumber.ToUpper())
                {
                    for (int i = 0; i < g.HotelStay.RoomList.Count(); i++)
                    {
                        if (g.HotelStay.RoomList[i].IsCheckedIn == false)
                        {
                            Stay s = new Stay(checkInDate, checkOutDate);

                            foreach (HotelRoom h in selectedRoomItemList)
                            {
                                s.AddRoom(h);
                            }
                            g.HotelStay = s;

                            for (int j = 0; j < g.HotelStay.RoomList.Count(); j++)
                            {
                                g.HotelStay.RoomList[j].IsAvail = false;
                                g.HotelStay.RoomList[j].IsCheckedIn = true;
                            }

                            roomsSelectedListView.ItemsSource = null;
                            selectedRoomItemList.Clear();
                        }
                        else
                        {
                            errorFlag = true;
                        }
                    }
                }
                else if (errorFlag == false)
                {
                    Stay s = new Stay(checkInDate, checkOutDate);

                    foreach (HotelRoom h in selectedRoomItemList)
                    {
                        s.AddRoom(h);
                    }

                    newGuestList.Add(new Guest { Name = guestName, PpNumber = guestPassportNumber, HotelStay = s, Membership = new Membership { Status = "Ordinary", Points = 0 }, IsCheckedIn = true });
                }
            }

            if (newGuestList.Count() != 0)
            {
                foreach (Guest n in newGuestList)
                {
                    for (int i = 0; i < n.HotelStay.RoomList.Count(); i++)
                    {
                        n.HotelStay.RoomList[i].IsAvail = false;
                        n.HotelStay.RoomList[i].IsCheckedIn = true;
                    }
                    guestDetailList.Add(n);

                    roomsSelectedListView.ItemsSource = null;
                    selectedRoomItemList.Clear();
                }
            }


            if (errorFlag == true)
            {
                MessageBox.Show("Check-Out all other rooms of the current user before proceeding.", "User has already checked in!");
            }
        }       

        /*Check-Out Button*/
        private void Button_Click_5(object sender, RoutedEventArgs e)
        {
            HotelRoom selectedRoomItem = (HotelRoom)availableRoomsListView.SelectedItem;

            string guestPassportNumber = guestPassportNumberTextBox.Text;

            if (selectedRoomItem.BedConfiguration == "Single" && selectedRoomItem.RoomType == "Standard")
            {
                ((StandardRoom)selectedRoomItem).RequireBreakfast = false;
                ((StandardRoom)selectedRoomItem).RequireWifi = false;
                selectedRoomItem.DailyRate = 90;
            }
            else if (selectedRoomItem.BedConfiguration == "Twin" && selectedRoomItem.RoomType == "Standard")
            {
                ((StandardRoom)selectedRoomItem).RequireBreakfast = false;
                ((StandardRoom)selectedRoomItem).RequireWifi = false;
                selectedRoomItem.DailyRate = 110;
            }
            else if (selectedRoomItem.BedConfiguration == "Triple" && selectedRoomItem.RoomType == "Standard")
            {
                ((StandardRoom)selectedRoomItem).RequireBreakfast = false;
                ((StandardRoom)selectedRoomItem).RequireWifi = false;
                selectedRoomItem.DailyRate = 120;
            }
            else if (selectedRoomItem.BedConfiguration == "Twin" && selectedRoomItem.RoomType == "Deluxe")
            {
                ((DeluxeRoom)selectedRoomItem).AdditionalBed = false;
                selectedRoomItem.DailyRate = 140;
            }
            else if (selectedRoomItem.BedConfiguration == "Triple" && selectedRoomItem.RoomType == "Deluxe")
            {
                ((DeluxeRoom)selectedRoomItem).AdditionalBed = false;
                selectedRoomItem.DailyRate = 210;
            }

            foreach (Guest g in guestDetailList)
            {
                if (g.PpNumber == guestPassportNumber.ToUpper())
                {
                    g.Membership.Points += Convert.ToInt32(g.HotelStay.CalculateTotal() / 10);

                    if (g.Membership.Points >= 200)
                    {
                        g.Membership.Status = "Gold";
                    }
                    else if (g.Membership.Points >= 100)
                    {
                        g.Membership.Status = "Silver";
                    }
                    for (int i = 0; i < g.HotelStay.RoomList.Count(); i++)
                    {
                        g.HotelStay.RoomList[i].IsAvail = true;
                        g.HotelStay.RoomList[i].IsCheckedIn = false;
                    }

                    g.HotelStay.RoomList.Remove(selectedRoomItem);
                    g.HotelStay = new Stay();
                }
            }

            hotelRoomList.Add(selectedRoomItem);
            guestDetailList.Remove(selectedRoomItem);
            availableRoomsListView.ItemsSource = null;

            MessageBox.Show("", "Check-Out is Successful!");

            Button_Click_4(sender, e);
        }

        /*Redeem Points Button*/
        private void RedeemPointsButton_Click(object sender, RoutedEventArgs e)
        {
            string guestPassportNumber = guestPassportNumberTextBox.Text;

            foreach (Guest g in guestDetailList)
            {
                if (g.PpNumber == guestPassportNumber.ToUpper())
                {
                    if(g.Membership.Status == "Gold")
                    {
                        int memberPrice = Convert.ToInt32(g.HotelStay.CalculateTotal() - g.Membership.Points);
                    }
                }
            }
            Button_Click_4(sender, e);
        }
    }
}