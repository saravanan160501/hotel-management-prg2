﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2assignment
{
    class Membership
    {
        private string status;

        private int points;

        public string Status
        {
            get { return status; }
            set { status = value; }
        }

        public int Points
        {
            get { return points; }
            set { points = value; }
        }

        public Membership() { }

        public Membership(string s,int p)
        {
            status = s;
            points = p;
        }

        public void EarnPoints(double e)
        {
           
            e += points;

        }

        public bool RedeemPoints(int rp)
        {
            if (rp <= points)
            {
                points -= rp;
                return true;
            }
            else
            {
                return false;
            }
        }

        public override string ToString()
        {
            return "Status:" + status + "Points:" + points;
        }
    }
}
