﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2assignment
{
    class StandardRoom:HotelRoom
    {
        private bool requireWifi;

        private bool requireBreakfast;

        public bool RequireWifi
        {
            get { return requireWifi; }
            set { requireWifi = value; }
        }

        public bool RequireBreakfast
        {
            get { return requireBreakfast; }
            set { requireBreakfast = value; }
        }

        public StandardRoom() { }

        public StandardRoom(string rt,string rn,string bc,double dr,bool ia,int noo)
        {
            RoomType = rt;
            RoomNumber = rn;
            BedConfiguration = bc;
            DailyRate = dr;
            IsAvail = ia;
            NoOfOccupants = noo;
        }

        public override double CalculateCharges()
        {
            double total = DailyRate;
            if (requireWifi == true)
            {
                total += 10;
            }
            if (requireBreakfast == true)
            {
                total += 20;
            }
            return total;
        }

        public override string ToString()
        {
            return "\nBreakfast Status: " + requireBreakfast + "\nWifi Status: " + requireWifi;
        }
    }
}
