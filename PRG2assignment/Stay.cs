﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2assignment
{
    class Stay
    {
        private List<HotelRoom> roomlist = new List<HotelRoom>();
        public List<HotelRoom> RoomList
        {
            get { return roomlist; }
            set { roomlist = value; }
        }

        private DateTime checkInDate;
        public DateTime CheckInDate
        {
            get { return checkInDate; }
            set { checkInDate = value; }
        }

        private DateTime checkOutDate;
        public DateTime CheckOutDate
        {
            get { return checkOutDate; }
            set { checkOutDate = value; }
        }

        public Stay() { }

        public Stay(DateTime ci,DateTime co)
        {
            checkInDate = ci;
            checkOutDate = co;
        }

        public void AddRoom(HotelRoom h)
        {
            roomlist.Add(h);
        }

        public double CalculateTotal()
        {
            double total = 0;
            foreach (HotelRoom h in roomlist)
            {
                total += Convert.ToDouble(h.DailyRate * (CheckOutDate - CheckInDate).TotalDays);
            }
            return total;
        }

        public override string ToString()
        {
            int numberOfNights = (CheckOutDate.Date - CheckInDate.Date).Days;
            return "\n\nNumber of Nights: " + numberOfNights + "\n\nTotal Amount: $" + CalculateTotal();
        }

    }
}
