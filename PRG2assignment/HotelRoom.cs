﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PRG2assignment
{
    abstract class HotelRoom:Guest
    {
        private string roomtype;
        public string RoomType
        {
            get { return roomtype; }
            set { roomtype = value; }
        }

        private string roomNumber;
        public string RoomNumber
        {
            get { return roomNumber; }
            set { roomNumber = value; }
        }

        private string bedConfiguration;
        public string BedConfiguration
        {
            get { return bedConfiguration; }
            set { bedConfiguration = value; }
        }

        private double dailyRate;
        public double DailyRate
        {
            get { return dailyRate; }
            set { dailyRate = value; }
        }

        private bool isAvail;
        public bool IsAvail
        {
            get { return isAvail; }
            set { isAvail = value; }
        }

        private int noOfOccupants;
        public int NoOfOccupants
        {
            get { return noOfOccupants; }
            set { noOfOccupants = value; }
        }

        public HotelRoom() { }
        public HotelRoom(string rt,string rn,string bc,int dr,bool ia,int noo)
        {
            roomtype = rt;
            roomNumber = rn;
            bedConfiguration = bc;
            dailyRate = dr;
            isAvail = ia;
            noOfOccupants = noo;
        }

        public abstract double CalculateCharges();
        
        public override string ToString()
        {
            return ("Room type:" + roomtype + "Room Number" + RoomNumber + "Bed Configuration" + bedConfiguration + "Daily Rate" + dailyRate + "Is Available?" + isAvail);
        }
    }
}
